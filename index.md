---
title: Quality Assurance Using A.I.
description: Use A.I. to enhance construction Quality
author: Youssef M. Abbass  
keywords: marp,marp-cli,slide
url: https://rwth-crmasters-sose22.gitlab.io/shared-runner-group/quality-assurance-concept/#3
marp: true
image: 
---

# Quality Assurance Concept
## Using A.I.

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected stories
### From a personal experiance 🏗 in construction site

- Many structrual elements should to be made in absolute precise way but at some point many defaults occurs while executing. 
- When there are errors existing this might cause a delay in the project and would cost more money to be rectified.
- Many Quality Assurance Procedures can be done remotely and more accuratley as its risky sometime to check the work done.
- Due to man error many faults may be considered as fatal mistake.
- Sometimes the errors are difficult to be spotted. 


---

# Mindmap
::: fence

@startuml

@startmindmap

* Construction Quality Using A.I.
**[#Orange] Contracting                        
*** Excavation
*** Plan Survey 
***[#Orange] Construction Activities 
****[#Orange] Scaffolding 
****[#Orange] Wooden Forms 
****[#Orange] Reinforcement Detailing 
*** Casting Concrete
*** Back Fill
*** Leveling
** Quality Inspection 
*** Compare to BIM


@endmindmap

@enduml

:::

---
# What Can A.I Do?

- Spot The errors made during construction of the structural elements.
- Having more accessibilty to areas with less capability to be accessed by man. 
- Can be used to monitor the building process remotely.
- Can be used to fix some defaults remotley by a mechanical arm.
- This machine can be operated Remotely 
---

# Open-source & hardware secondary research

[https://github.com/epictos/epictos.github.io/blob/e7247364d431800e084e9f00c97c3369783f937b/quality-assurance-in-construction-industry-pdf/index.html)](https://github.com/epictos/epictos.github.io/blob/e7247364d431800e084e9f00c97c3369783f937b/quality-assurance-in-construction-industry-pdf/index.html)

- This is code on HTML that shows a code for Construction Quality Assurance. 

### Repository for Arm Navigation
[https://github.com/AtsushiSakai/PythonRobotics#arm-navigation](https://github.com/AtsushiSakai/PythonRobotics#arm-navigation)
 


---

# Locked concept

- Attempt to use computer vision and machine learning detect defaults construction site.
- A.I. Should Compare the work done with the bim data to Assure Quality. 
- Combine BIM with A.I to work side by side in Co-ordination.
- Use sensor technology

